
CC=clang
CFLAGS=-ggdb
LDFLAGS=-lm

ray: ray.cpp
	${CC} ${CFLAGS} -o $@ $< ${LDFLAGS}

ray.ppm: ray
	./ray > ray.ppm

view: ray.ppm
	sxiv ray.ppm
