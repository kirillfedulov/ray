# Ray tracing in C++ for learning purposes

Building & Running:
	make && ./ray > ray.ppm

Then open ray.ppm with your favourite image viewer

![](ray.png)
