#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

union Tuple {
	struct {
		float x, y, z, w;
	};

	struct {
		float r, g, b, a;
	};
};

typedef Tuple Vector;
typedef Tuple Point;
typedef Tuple Color;

Vector vector(float x, float y, float z) {
	Vector result = {x, y, z, 0.0f};
	return result;
}

Point point(float x, float y, float z) {
	Point result = {x, y, z, 1.0f};
	return result;
}

Color color(float r, float g, float b) {
	Color result = {r, g, b, 0.0f};
	return result;
}

Tuple operator+(const Tuple &a, const Tuple &b) {
	Tuple result = {a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w};
	return result;
}

Tuple &operator+=(Tuple &a, const Tuple &b) {
	a.x += b.x;
	a.y += b.y;
	a.z += b.z;
	a.w += b.w;

	return a;
}

Tuple operator-(const Tuple &a, const Tuple &b) {
	Tuple result = {a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w};
	return result;
}

Tuple operator-(const Tuple &a) {
	Tuple result = {-a.x, -a.y, -a.z, -a.w};
	return result;
}

Tuple operator*(const Tuple &a, const Tuple &b) {
	Tuple result = {a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w};
	return result;
}

Tuple operator*(const Tuple &a, float b) {
	Tuple result = {a.x * b, a.y * b, a.z * b, a.w * b};
	return result;
}

Tuple operator*(float a, const Tuple &b) {
	Tuple result = b * a;
	return result;
}

bool feq(float a, float b) {
	bool result = fabs(a - b) < 1e-4;
	return result;
}

Tuple operator/(const Tuple &a, float b) {
	assert(!feq(b, 0.0f));

	Tuple result = {a.x / b, a.y / b, a.z / b, a.w / b};
	return result;
}

bool operator==(const Tuple &a, const Tuple &b) {
	bool result = feq(a.x, b.x) && feq(a.y, b.y) && feq(a.z, b.z) && feq(a.w, b.w);
	return result;
}

float length(const Tuple &a) {
	float result = sqrtf(a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w);
	return result;
}

float length_squared(const Tuple &a) {
	float result = a.x * a.x + a.y * a.y + a.z * a.z + a.w * a.w;
	return result;
}

Tuple normalized(const Tuple &a) {
	float len = length(a);
	assert(!feq(len, 0.0f));

	Tuple result = {a.x / len, a.y / len, a.z / len, 0.0f};
	return result;
}

float dot(const Tuple &a, const Tuple &b) {
	float result = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
	return result;
}

Tuple cross(const Tuple &a, const Tuple &b) {
	Tuple result = {a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x, 0.0f};
	return result;
}

struct Canvas {
	int width, height;
	Color *colors;


	Canvas(int width, int height) : width(width), height(height) {
		colors = (Color *) calloc(width * height, sizeof(Color));
	}
};

void writepix(Canvas &canvas, int x, int y, Color color) {
	assert(0 <= x && x < canvas.width);
	assert(0 <= y && y < canvas.height);

	color.r = color.r < 0.0f ? 0.0f : (color.r > 1.0f ? 1.0f : color.r);
	color.g = color.g < 0.0f ? 0.0f : (color.g > 1.0f ? 1.0f : color.g);
	color.b = color.b < 0.0f ? 0.0f : (color.b > 1.0f ? 1.0f : color.b);
	canvas.colors[y * canvas.width + x] = color;
}

Color &pixat(Canvas &canvas, int x, int y) {
	assert(0 <= x && x < canvas.width);
	assert(0 <= y && y < canvas.height);

	Color &result = canvas.colors[y * canvas.width + x];
	return result;
}

void fill(Canvas &canvas, const Color &color) {
	for (int y = 0; y < canvas.height; y++) {
		for (int x = 0; x < canvas.width; x++) {
			canvas.colors[y * canvas.width + x] = color;
		}
	}
}

void toppm(const Canvas &canvas, FILE *fp) {
	fprintf(fp, "P3\n");
	fprintf(fp, "%d %d\n", canvas.width, canvas.height);
	fprintf(fp, "255\n");
	for (int y = 0; y < canvas.height; y++) {
		for (int x = 0; x < canvas.width; x++) {
			Color &color = canvas.colors[y * canvas.width + x];
			int r = color.r * 255.0f;
			int g = color.g * 255.0f;
			int b = color.b * 255.0f;
			fprintf(fp, "%d %d %d ", r, g, b);
		}
		fprintf(fp, "\n");
	}
}

struct Matrix2 {
	float e[2][2];
};

bool operator==(const Matrix2 &a, const Matrix2 &b) {
	bool result =
		feq(a.e[0][0], b.e[0][0]) &&
		feq(a.e[0][1], b.e[0][1]) &&
		feq(a.e[1][0], b.e[1][0]) &&
		feq(a.e[1][1], b.e[1][1]);
	return result;
}

Matrix2 matrix2(float e00, float e01,
	        float e10, float e11) {
	Matrix2 result;
	result.e[0][0] = e00;
	result.e[0][1] = e01;
	result.e[1][0] = e10;
	result.e[1][1] = e11;

	return result;
}

float det2(const Matrix2 &a) {
	float result = a.e[0][0] * a.e[1][1] - a.e[0][1] * a.e[1][0];
	return result;
}

struct Matrix3 {
	float e[3][3];
};

bool operator==(const Matrix3 &a, const Matrix3 &b) {
	bool result =
		feq(a.e[0][0], b.e[0][0]) &&
		feq(a.e[0][1], b.e[0][1]) &&
		feq(a.e[0][2], b.e[0][2]) &&
		feq(a.e[1][0], b.e[1][0]) &&
		feq(a.e[1][1], b.e[1][1]) &&
		feq(a.e[1][2], b.e[1][2]) &&
		feq(a.e[2][0], b.e[2][0]) &&
		feq(a.e[2][1], b.e[2][1]) &&
		feq(a.e[2][2], b.e[2][2]);
	return result;
}

Matrix3 matrix3(float e00, float e01, float e02,
	        float e10, float e11, float e12,
	        float e20, float e21, float e22) {
	Matrix3 result;
	result.e[0][0] = e00;
	result.e[0][1] = e01;
	result.e[0][2] = e02;
	result.e[1][0] = e10;
	result.e[1][1] = e11;
	result.e[1][2] = e12;
	result.e[2][0] = e20;
	result.e[2][1] = e21;
	result.e[2][2] = e22;

	return result;
}

float det3(const Matrix3 &a) {
	float result = 0.0f;
	result +=  a.e[0][0] * (a.e[1][1] * a.e[2][2] - a.e[1][2] * a.e[2][1]);
	result += -a.e[0][1] * (a.e[1][0] * a.e[2][2] - a.e[1][2] * a.e[2][0]);
	result +=  a.e[0][2] * (a.e[1][0] * a.e[2][1] - a.e[1][1] * a.e[2][0]);

	return result;
}

struct Matrix4 {
	float e[4][4];
};

bool operator==(const Matrix4 &a, const Matrix4 &b) {
	bool result =
		feq(a.e[0][0], b.e[0][0]) &&
		feq(a.e[0][1], b.e[0][1]) &&
		feq(a.e[0][2], b.e[0][2]) &&
		feq(a.e[0][3], b.e[0][3]) &&
		feq(a.e[1][0], b.e[1][0]) &&
		feq(a.e[1][1], b.e[1][1]) &&
		feq(a.e[1][2], b.e[1][2]) &&
		feq(a.e[1][3], b.e[1][3]) &&
		feq(a.e[2][0], b.e[2][0]) &&
		feq(a.e[2][1], b.e[2][1]) &&
		feq(a.e[2][2], b.e[2][2]) &&
		feq(a.e[2][3], b.e[2][3]) &&
		feq(a.e[3][0], b.e[3][0]) &&
		feq(a.e[3][1], b.e[3][1]) &&
		feq(a.e[3][2], b.e[3][2]) &&
		feq(a.e[3][3], b.e[3][3]);

	return result;
}

Matrix4 matrix4(float e00, float e01, float e02, float e03,
	        float e10, float e11, float e12, float e13,
	        float e20, float e21, float e22, float e23,
	        float e30, float e31, float e32, float e33) {
	Matrix4 result;
	result.e[0][0] = e00;
	result.e[0][1] = e01;
	result.e[0][2] = e02;
	result.e[0][3] = e03;
	result.e[1][0] = e10;
	result.e[1][1] = e11;
	result.e[1][2] = e12;
	result.e[1][3] = e13;
	result.e[2][0] = e20;
	result.e[2][1] = e21;
	result.e[2][2] = e22;
	result.e[2][3] = e23;
	result.e[3][0] = e30;
	result.e[3][1] = e31;
	result.e[3][2] = e32;
	result.e[3][3] = e33;

	return result;
}

Matrix4 operator*(const Matrix4 &a, const Matrix4 &b) {
	Matrix4 result;
	result.e[0][0] = a.e[0][0] * b.e[0][0] + a.e[0][1] * b.e[1][0] + a.e[0][2] * b.e[2][0] + a.e[0][3] * b.e[3][0];
	result.e[0][1] = a.e[0][0] * b.e[0][1] + a.e[0][1] * b.e[1][1] + a.e[0][2] * b.e[2][1] + a.e[0][3] * b.e[3][1];
	result.e[0][2] = a.e[0][0] * b.e[0][2] + a.e[0][1] * b.e[1][2] + a.e[0][2] * b.e[2][2] + a.e[0][3] * b.e[3][2];
	result.e[0][3] = a.e[0][0] * b.e[0][3] + a.e[0][1] * b.e[1][3] + a.e[0][2] * b.e[2][3] + a.e[0][3] * b.e[3][3];
	result.e[1][0] = a.e[1][0] * b.e[0][0] + a.e[1][1] * b.e[1][0] + a.e[1][2] * b.e[2][0] + a.e[1][3] * b.e[3][0];
	result.e[1][1] = a.e[1][0] * b.e[0][1] + a.e[1][1] * b.e[1][1] + a.e[1][2] * b.e[2][1] + a.e[1][3] * b.e[3][1];
	result.e[1][2] = a.e[1][0] * b.e[0][2] + a.e[1][1] * b.e[1][2] + a.e[1][2] * b.e[2][2] + a.e[1][3] * b.e[3][2];
	result.e[1][3] = a.e[1][0] * b.e[0][3] + a.e[1][1] * b.e[1][3] + a.e[1][2] * b.e[2][3] + a.e[1][3] * b.e[3][3];
	result.e[2][0] = a.e[2][0] * b.e[0][0] + a.e[2][1] * b.e[1][0] + a.e[2][2] * b.e[2][0] + a.e[2][3] * b.e[3][0];
	result.e[2][1] = a.e[2][0] * b.e[0][1] + a.e[2][1] * b.e[1][1] + a.e[2][2] * b.e[2][1] + a.e[2][3] * b.e[3][1];
	result.e[2][2] = a.e[2][0] * b.e[0][2] + a.e[2][1] * b.e[1][2] + a.e[2][2] * b.e[2][2] + a.e[2][3] * b.e[3][2];
	result.e[2][3] = a.e[2][0] * b.e[0][3] + a.e[2][1] * b.e[1][3] + a.e[2][2] * b.e[2][3] + a.e[2][3] * b.e[3][3];
	result.e[3][0] = a.e[3][0] * b.e[0][0] + a.e[3][1] * b.e[1][0] + a.e[3][2] * b.e[2][0] + a.e[3][3] * b.e[3][0];
	result.e[3][1] = a.e[3][0] * b.e[0][1] + a.e[3][1] * b.e[1][1] + a.e[3][2] * b.e[2][1] + a.e[3][3] * b.e[3][1];
	result.e[3][2] = a.e[3][0] * b.e[0][2] + a.e[3][1] * b.e[1][2] + a.e[3][2] * b.e[2][2] + a.e[3][3] * b.e[3][2];
	result.e[3][3] = a.e[3][0] * b.e[0][3] + a.e[3][1] * b.e[1][3] + a.e[3][2] * b.e[2][3] + a.e[3][3] * b.e[3][3];

	return result;
}

Vector operator*(const Matrix4 &a, const Vector &b) {
	Vector result;
	result.x = a.e[0][0] * b.x + a.e[0][1] * b.y + a.e[0][2] * b.z + a.e[0][3] * b.w;
	result.y = a.e[1][0] * b.x + a.e[1][1] * b.y + a.e[1][2] * b.z + a.e[1][3] * b.w;
	result.z = a.e[2][0] * b.x + a.e[2][1] * b.y + a.e[2][2] * b.z + a.e[2][3] * b.w;
	result.w = a.e[3][0] * b.x + a.e[3][1] * b.y + a.e[3][2] * b.z + a.e[3][3] * b.w;

	return result;
}

Matrix4 identity4() {
	Matrix4 result = matrix4(1.0f, 0.0f, 0.0f, 0.0f,
			         0.0f, 1.0f, 0.0f, 0.0f,
			         0.0f, 0.0f, 1.0f, 0.0f,
			         0.0f, 0.0f, 0.0f, 1.0f);

	return result;
}

Matrix4 transpose4(const Matrix4 &a) {
	Matrix4 result;
	result.e[0][0] = a.e[0][0];
	result.e[0][1] = a.e[1][0];
	result.e[0][2] = a.e[2][0];
	result.e[0][3] = a.e[3][0];
	result.e[1][0] = a.e[0][1];
	result.e[1][1] = a.e[1][1];
	result.e[1][2] = a.e[2][1];
	result.e[1][3] = a.e[3][1];
	result.e[2][0] = a.e[0][2];
	result.e[2][1] = a.e[1][2];
	result.e[2][2] = a.e[2][2];
	result.e[2][3] = a.e[3][2];
	result.e[3][0] = a.e[0][3];
	result.e[3][1] = a.e[1][3];
	result.e[3][2] = a.e[2][3];
	result.e[3][3] = a.e[3][3];

	return result;
}

float det4(const Matrix4 &a) {
	float result = 0.0f;
	result +=  a.e[0][0] * (a.e[1][1] * (a.e[2][2] * a.e[3][3] - a.e[2][3] * a.e[3][2]) - a.e[1][2] * (a.e[2][1] * a.e[3][3] - a.e[2][3] * a.e[3][1]) + a.e[1][3] * (a.e[2][1] * a.e[3][2] - a.e[2][2] * a.e[3][1]));
	result += -a.e[0][1] * (a.e[1][0] * (a.e[2][2] * a.e[3][3] - a.e[2][3] * a.e[3][2]) - a.e[1][2] * (a.e[2][0] * a.e[3][3] - a.e[2][3] * a.e[3][0]) + a.e[1][3] * (a.e[2][0] * a.e[3][2] - a.e[2][2] * a.e[3][0]));
	result +=  a.e[0][2] * (a.e[1][0] * (a.e[2][1] * a.e[3][3] - a.e[2][3] * a.e[3][1]) - a.e[1][1] * (a.e[2][0] * a.e[3][3] - a.e[2][3] * a.e[3][0]) + a.e[1][3] * (a.e[2][0] * a.e[3][1] - a.e[2][1] * a.e[3][0]));
	result += -a.e[0][3] * (a.e[1][0] * (a.e[2][1] * a.e[3][2] - a.e[2][2] * a.e[3][1]) - a.e[1][1] * (a.e[2][0] * a.e[3][2] - a.e[2][2] * a.e[3][0]) + a.e[1][2] * (a.e[2][0] * a.e[3][1] - a.e[2][1] * a.e[3][0]));

	return result;
}

Matrix4 inverse4(const Matrix4 &a) {
	float det = det4(a);
	assert(!feq(det, 0.0f));

	Matrix4 result;
	result.e[0][0] =  (a.e[1][1] * (a.e[2][2] * a.e[3][3] - a.e[2][3] * a.e[3][2]) - a.e[1][2] * (a.e[2][1] * a.e[3][3] - a.e[2][3] * a.e[3][1]) + a.e[1][3] * (a.e[2][1] * a.e[3][2] - a.e[2][2] * a.e[3][1])) / det;
	result.e[1][0] = -(a.e[1][0] * (a.e[2][2] * a.e[3][3] - a.e[2][3] * a.e[3][2]) - a.e[1][2] * (a.e[2][0] * a.e[3][3] - a.e[2][3] * a.e[3][0]) + a.e[1][3] * (a.e[2][0] * a.e[3][2] - a.e[2][2] * a.e[3][0])) / det;
	result.e[2][0] =  (a.e[1][0] * (a.e[2][1] * a.e[3][3] - a.e[2][3] * a.e[3][1]) - a.e[1][1] * (a.e[2][0] * a.e[3][3] - a.e[2][3] * a.e[3][0]) + a.e[1][3] * (a.e[2][0] * a.e[3][1] - a.e[2][1] * a.e[3][0])) / det;
	result.e[3][0] = -(a.e[1][0] * (a.e[2][1] * a.e[3][2] - a.e[2][2] * a.e[3][1]) - a.e[1][1] * (a.e[2][0] * a.e[3][2] - a.e[2][2] * a.e[3][0]) + a.e[1][2] * (a.e[2][0] * a.e[3][1] - a.e[2][1] * a.e[3][0])) / det;
	result.e[0][1] = -(a.e[0][1] * (a.e[2][2] * a.e[3][3] - a.e[2][3] * a.e[3][2]) - a.e[0][2] * (a.e[2][1] * a.e[3][3] - a.e[2][3] * a.e[3][1]) + a.e[0][3] * (a.e[2][1] * a.e[3][2] - a.e[2][2] * a.e[3][1])) / det;
	result.e[1][1] =  (a.e[0][0] * (a.e[2][2] * a.e[3][3] - a.e[2][3] * a.e[3][2]) - a.e[0][2] * (a.e[2][0] * a.e[3][3] - a.e[2][3] * a.e[3][0]) + a.e[0][3] * (a.e[2][0] * a.e[3][2] - a.e[2][2] * a.e[3][0])) / det;
	result.e[2][1] = -(a.e[0][0] * (a.e[2][1] * a.e[3][3] - a.e[2][3] * a.e[3][1]) - a.e[0][1] * (a.e[2][0] * a.e[3][3] - a.e[2][3] * a.e[3][0]) + a.e[0][3] * (a.e[2][0] * a.e[3][1] - a.e[2][1] * a.e[3][0])) / det;
	result.e[3][1] =  (a.e[0][0] * (a.e[2][1] * a.e[3][2] - a.e[2][2] * a.e[3][1]) - a.e[0][1] * (a.e[2][0] * a.e[3][2] - a.e[2][2] * a.e[3][0]) + a.e[0][2] * (a.e[2][0] * a.e[3][1] - a.e[2][1] * a.e[3][0])) / det;
	result.e[0][2] =  (a.e[0][1] * (a.e[1][2] * a.e[3][3] - a.e[1][3] * a.e[3][2]) - a.e[0][2] * (a.e[1][1] * a.e[3][3] - a.e[1][3] * a.e[3][1]) + a.e[0][3] * (a.e[1][1] * a.e[3][2] - a.e[1][2] * a.e[3][1])) / det;
	result.e[1][2] = -(a.e[0][0] * (a.e[1][2] * a.e[3][3] - a.e[1][3] * a.e[3][2]) - a.e[0][2] * (a.e[1][0] * a.e[3][3] - a.e[1][3] * a.e[3][0]) + a.e[0][3] * (a.e[1][0] * a.e[3][2] - a.e[1][2] * a.e[3][0])) / det;
	result.e[2][2] =  (a.e[0][0] * (a.e[1][1] * a.e[3][3] - a.e[1][3] * a.e[3][1]) - a.e[0][1] * (a.e[1][0] * a.e[3][3] - a.e[1][3] * a.e[3][0]) + a.e[0][3] * (a.e[1][0] * a.e[3][1] - a.e[1][1] * a.e[3][0])) / det;
	result.e[3][2] = -(a.e[0][0] * (a.e[1][1] * a.e[3][2] - a.e[1][2] * a.e[3][1]) - a.e[0][1] * (a.e[1][0] * a.e[3][2] - a.e[1][2] * a.e[3][0]) + a.e[0][2] * (a.e[1][0] * a.e[3][1] - a.e[1][1] * a.e[3][0])) / det;
	result.e[0][3] = -(a.e[0][1] * (a.e[1][2] * a.e[2][3] - a.e[1][3] * a.e[2][2]) - a.e[0][2] * (a.e[1][1] * a.e[2][3] - a.e[1][3] * a.e[2][1]) + a.e[0][3] * (a.e[1][1] * a.e[2][2] - a.e[1][2] * a.e[2][1])) / det;
	result.e[1][3] =  (a.e[0][0] * (a.e[1][2] * a.e[2][3] - a.e[1][3] * a.e[2][2]) - a.e[0][2] * (a.e[1][0] * a.e[2][3] - a.e[1][3] * a.e[2][0]) + a.e[0][3] * (a.e[1][0] * a.e[2][2] - a.e[1][2] * a.e[2][0])) / det;
	result.e[2][3] = -(a.e[0][0] * (a.e[1][1] * a.e[2][3] - a.e[1][3] * a.e[2][1]) - a.e[0][1] * (a.e[1][0] * a.e[2][3] - a.e[1][3] * a.e[2][0]) + a.e[0][3] * (a.e[1][0] * a.e[2][1] - a.e[1][1] * a.e[2][0])) / det;
	result.e[3][3] =  (a.e[0][0] * (a.e[1][1] * a.e[2][2] - a.e[1][2] * a.e[2][1]) - a.e[0][1] * (a.e[1][0] * a.e[2][2] - a.e[1][2] * a.e[2][0]) + a.e[0][2] * (a.e[1][0] * a.e[2][1] - a.e[1][1] * a.e[2][0])) / det;

	return result;
}

void mtest() {
	Matrix4 m = matrix4(1.0f, 2.0f, 3.0f, 4.0f,
			    5.0f, 6.0f, 7.0f, 8.0f,
			    9.0f, 1.0f, 2.0f, 3.0f,
			    4.0f, 5.0f, 6.0f, 7.0f);
	Matrix4 r = m * identity4();
	assert(m == r);

	Matrix4 t = transpose4(m);
	assert(t == matrix4(1.0f, 5.0f, 9.0f, 4.0f,
			    2.0f, 6.0f, 1.0f, 5.0f,
			    3.0f, 7.0f, 2.0f, 6.0f,
			    4.0f, 8.0f, 3.0f, 7.0f));

	assert(transpose4(identity4()) == identity4());

	assert(feq(det2(matrix2(1.0f, 5.0f,
			       -3.0f, 2.0f)), 17.0f));

	assert(feq(det3(matrix3(1.0f, 2.0f, 6.0f,
		               -5.0f, 8.0f, -4.0f,
			        2.0f, 6.0f, 4.0f)), -196.0f));

	assert(feq(det4(matrix4(-2.0f, -8.0f, 3.0f, 5.0f,
			        -3.0f, 1.0f, 7.0f, 3.0f,
			         1.0f, 2.0f, -9.0f, 6.0f,
			        -6.0f, 7.0f, 7.0f, -9.0f)), -4071.0f));

	assert(inverse4(matrix4(-5.0f, 2.0f, 6.0f, -8.0f,
			         1.0f, -5.0f, 1.0f, 8.0f,
			         7.0f, 7.0f, -6.0f, -7.0f,
			         1.0f, -3.0f, 7.0f, 4.0f)) == matrix4(0.21805f, 0.45113f, 0.24060f, -0.04511,
				                                  -0.80827f, -1.45677f, -0.44361f, 0.52068f,
								  -0.07895f, -0.22368f, -0.05263f, 0.19737f,
								  -0.52256f, -0.81391f, -0.30075f, 0.30639f));
	assert(inverse4(matrix4(8.0f, -5.0f, 9.0f, 2.0f,
                                 7.0f, 5.0f, 6.0f, 1.0f,
                                -6.0f, 0.0f, 9.0f, 6.0f,
                                -3.0f, 0.0f, -9.0f, -4.0f)) == matrix4(-0.15385f, -0.15385f, -0.28205f, -0.53846f,
                                                                       -0.07692f, 0.12308f, 0.02564f, 0.03077f,
                                                                        0.35897f, 0.35897f, 0.43590f, 0.92308f,
                                                                        -0.69231f, -0.69231f, -0.76923f, -1.92308f));

	assert(inverse4(matrix4(9.0f, 3.0f, 0.0f, 9.0f,
                               -5.0f, -2.0f, -6.0f, -3.0f,
                               -4.0f, 9.0f, 6.0f, 4.0f,
                               -7.0f, 6.0f, 6.0f, 2.0f)) == matrix4(-0.04074f, -0.07778f, 0.14444f, -0.22222f,
                                                                    -0.07778f, 0.03333f, 0.36667f, -0.33333f,
                                                                    -0.02901f, -0.14630f, -0.10926f, 0.12963f,
                                                                     0.17778f, 0.06667f, -0.26667f, 0.33333f));

	Matrix4 a = matrix4(3.0f, -9.0f, 7.0f, 3.0f,
                            3.0f, -8.0f, 2.0f, -9.0f,
                           -4.0f, 4.0f, 4.0f, 1.0f,
                           -6.0f, 5.0f, -1.0f, 1.0f);
	Matrix4 b = matrix4(8.0f, 2.0f, 2.0f, 2.0f,
                            3.0f, -1.0f, 7.0f, 0.0f,
                            7.0f, 0.0f, 5.0f, 4.0f,
                            6.0f, -2.0f, 0.0f, 5.0f);
	Matrix4 c = a * b;

	assert(c * inverse4(b) == a);

	assert(a * inverse4(a) == identity4());

	assert(transpose4(inverse4(a)) == inverse4(transpose4(a)));

	assert(identity4() == inverse4(identity4()));
}

Matrix4 translate(float x, float y, float z) {
	Matrix4 result = identity4();
	result.e[0][3] = x;
	result.e[1][3] = y;
	result.e[2][3] = z;

	return result;
}

Matrix4 scale(float x, float y, float z) {
	Matrix4 result = identity4();
	result.e[0][0] = x;
	result.e[1][1] = y;
	result.e[2][2] = z;

	return result;
}

Matrix4 rotatex(float rad) {
	Matrix4 result = identity4();
	float sinr = sinf(rad), cosr = cosf(rad);
	result.e[1][1] = cosr;
	result.e[1][2] = -sinr;
	result.e[2][1] = sinr;
	result.e[2][2] = cosr;

	return result;
}

Matrix4 rotatey(float rad) {
	Matrix4 result = identity4();
	float sinr = sinf(rad), cosr = cosf(rad);
	result.e[0][0] = cosr;
	result.e[0][2] = sinr;
	result.e[2][0] = -sinr;
	result.e[2][2] = cosr;

	return result;
}

Matrix4 rotatez(float rad) {
	Matrix4 result = identity4();
	float sinr = sinf(rad), cosr = cosf(rad);
	result.e[0][0] = cosr;
	result.e[0][1] = -sinr;
	result.e[1][0] = sinr;
	result.e[1][1] = cosr;

	return result;
}

Matrix4 shear(float xy, float xz, float yx, float yz, float zx, float zy) {
	Matrix4 result = identity4();
	result.e[0][1] = xy;
	result.e[0][2] = xz;
	result.e[1][0] = yx;
	result.e[1][2] = yz;
	result.e[2][0] = zx;
	result.e[2][1] = zy;

	return result;
}

void transtest() {
	assert(translate(5.0f, -3.0f, 2.0f) * point(-3.0f, 4.0f, 5.0f) == point(2.0f, 1.0f, 7.0f));
	assert(inverse4(translate(5.0f, -3.0f, 2.0f)) * point(-3.0f, 4.0f, 5.0f) == point(-8.0f, 7.0f, 3.0f));

	assert(scale(2.0f, 3.0f, 4.0f) * point(-4.0f, 6.0f, 8.0f) == point(-8.0f, 18.0f, 32.0f));
	assert(scale(2.0f, 3.0f, 4.0f) * vector(-4.0f, 6.0f, 8.0f) == vector(-8.0f, 18.0f, 32.0f));
	assert(inverse4(scale(2.0f, 3.0f, 4.0f)) * vector(-4.0f, 6.0f, 8.0f) == vector(-2.0f, 2.0f, 2.0f));
	assert(scale(-1.0f, 1.0f, 1.0f) * point(2.0f, 3.0f, 4.0f) == point(-2.0f, 3.0f, 4.0f));

	assert(rotatex(M_PI / 4.0f) * point(0.0f, 1.0f, 0.0f) == point(0.0f, sqrtf(2.0f) / 2.0f, sqrt(2.0f) / 2.0f));
	assert(rotatex(M_PI / 2.0f) * point(0.0f, 1.0f, 0.0f) == point(0.0f, 0.0f, 1.0f));
	assert(inverse4(rotatex(M_PI / 4.0f)) * point(0.0f, 1.0f, 0.0f) == point(0.0f, sqrtf(2.0f) / 2.0f, -sqrt(2.0f) / 2.0f));
	assert(rotatey(M_PI / 4.0f) * point(0.0f, 0.0f, 1.0f) == point(sqrtf(2.0f) / 2.0f, 0.0f, sqrt(2.0f) / 2.0f));
	assert(rotatey(M_PI / 2.0f) * point(0.0f, 0.0f, 1.0f) == point(1.0f, 0.0f, 0.0f));
	assert(rotatez(M_PI / 4.0f) * point(0.0f, 1.0f, 0.0f) == point(-sqrtf(2.0f) / 2.0f, sqrt(2.0f) / 2.0f, 0.0f));
	assert(rotatez(M_PI / 2.0f) * point(0.0f, 1.0f, 0.0f) == point(-1.0f, 0.0f, 0.0f));

	assert(shear(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f) * point(2.0f, 3.0f, 4.0f) == point(5.0f, 3.0f, 4.0f));
	assert(shear(0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f) * point(2.0f, 3.0f, 4.0f) == point(6.0f, 3.0f, 4.0f));
	assert(shear(0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f) * point(2.0f, 3.0f, 4.0f) == point(2.0f, 5.0f, 4.0f));
	assert(shear(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f) * point(2.0f, 3.0f, 4.0f) == point(2.0f, 7.0f, 4.0f));
	assert(shear(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f) * point(2.0f, 3.0f, 4.0f) == point(2.0f, 3.0f, 6.0f));
	assert(shear(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f) * point(2.0f, 3.0f, 4.0f) == point(2.0f, 3.0f, 7.0f));
}

struct Ray {
	Point origin;
	Vector direction;

	Ray(const Point &origin, const Vector &direction) : origin(origin), direction(direction) {}
};

struct Sphere {
	Point center;
	float radius;

	Sphere(const Point &center, float radius) : center(center), radius(radius) {}
};

bool ray_intersects_sphere(const Ray &ray, const Sphere &sphere, float &t1, float &t2) {
	Vector sphere_to_ray = ray.origin - sphere.center;

	float a = length_squared(ray.direction);
	float b = 2.0f * dot(sphere_to_ray, ray.direction);
	float c = length_squared(sphere_to_ray) - sphere.radius * sphere.radius;
	float D = b * b - 4.0f * a * c;
	if (D >= 0.0f) {
		float D_sqrt = sqrtf(D);
		t1 = (-b - D_sqrt) / (2.0f * a);
		t2 = (-b + D_sqrt) / (2.0f * a);
		return true;
	}

	return false;
}

struct PointLight {
	Point position;
	Color intensity;

	PointLight(const Point &position, const Color &intensity) : position(position), intensity(intensity) {}
};

struct Material {
	Color color;
	float ambient;
	float diffuse;
	float specular;
	float shininess;

	Material(const Color &color, float ambient, float diffuse, float specular, float shininess)
		: color(color), ambient(ambient), diffuse(diffuse), specular(specular), shininess(shininess) {}
};

Matrix4 look_at(const Point &eye, const Point &target, const Vector &up) {
	Vector forward = normalized(target - eye);
	Vector left = cross(forward, normalized(up));
	Vector true_up = cross(left, forward);
	Matrix4 orientation = matrix4(left.x,      left.y,     left.z,    0.0f,
			              true_up.x,   true_up.y,  true_up.z, 0.0f,
				      -forward.x, -forward.y, -forward.z, 0.0f,
				      0.0f,        0.0f,        0.0f,     1.0f);

	Matrix4 result = orientation * translate(-eye.x, -eye.y, -eye.z);
	return result;
}

void viewtest() {
	assert(look_at(point(0.0f, 0.0f, 0.0f), point(0.0f, 0.0f, -1.0f), vector(0.0f, 1.0f, 0.0f)) == identity4());
	assert(look_at(point(0.0f, 0.0f, 0.0f), point(0.0f, 0.0f, 1.0f), vector(0.0f, 1.0f, 0.0f)) == scale(-1.0f, 1.0f, -1.0f));
	assert(look_at(point(0.0f, 0.0f, 8.0f), point(0.0f, 0.0f, 0.0f), vector(0.0f, 1.0f, 0.0f)) == translate(0.0f, 0.0f, -8.0f));
	assert(look_at(point(1.0f, 3.0f, 2.0f), point(4.0f, -2.0f, 8.0f), vector(1.0f, 1.0f, 0.0f)) ==
	       matrix4(-0.50709 ,0.50709 ,0.67612 ,-2.36643,
                        0.76772 ,0.60609 ,0.12122 ,-2.82843,
                       -0.35857 ,0.59761 ,-0.71714 ,0.00000,
                        0.00000 ,0.00000 ,0.00000 ,1.000000));

}

int main() {
	mtest();
	transtest();
	viewtest();

	Canvas canvas = Canvas(1280, 720);
	fill(canvas, color(0.2f, 0.2f, 0.2f));

	Point camera_position = point(0.0f, 0.0f, 0.0f);
	float viewport_width = (float) canvas.width / (float) canvas.height;
	float viewport_height = 1.0f;
	float viewport_distance_from_camera = 1.0f;
	float max_view_distance = 9.0f;

	const int SPHERES_COUNT = 3;
	Sphere spheres[SPHERES_COUNT] = {
		Sphere(point(-4.0f, 1.0f, 4.0f), 1.0f),
		Sphere(point(0.0f, 0.0f, 3.0f), 1.0f),
		Sphere(point(2.0f, -1.0f, 2.0f), 0.6f)
	};

	const int MATERIALS_COUNT = 3;
	Material materials[MATERIALS_COUNT] = {
		Material(color(1.0f, 0.0f, 1.0f), 0.2f, 0.9f, 1.0f, 1000.0f),
		Material(color(0.1f, 0.6f, 0.0f), 0.2f, 1.0f, 1.0f, 100.0f),
		Material(color(1.0f, 1.0f, 0.0f), 0.2f, 0.9f, 1.0f, 10.0f)
	};

	const int LIGHTS_COUNT = 2;
	PointLight lights[LIGHTS_COUNT] = {
		PointLight(point(4.0f, 2.0f, 0.0f), color(0.4f, 0.4f, 0.4f)),
		PointLight(point(-4.0f, -1.0f, 0.0f), color(0.3f, 0.0f, 0.0f)),
	};

	for (int y = 0; y < canvas.height; y++) {
		for (int x = 0; x < canvas.width; x++) {
			Point viewport_position = point((x - (canvas.width - 1) / 2.0f) * viewport_width / (canvas.width / 2.0f),
							(canvas.height - (y + (canvas.height + 1) / 2.0f)) * viewport_height / (canvas.height / 2.0f),
							viewport_distance_from_camera);
			Ray ray = Ray(camera_position, normalized(viewport_position - camera_position));
#if 0
			Matrix4 transform = scale(1.0f, 2.5f, 1.0f);
			ray.origin = transform * ray.origin;
			ray.direction = transform * ray.direction;
#endif

			for (int i = 0; i < SPHERES_COUNT; i++) {
				const Sphere &sphere = spheres[i];

				float t1 = 0.0f, t2 = 0.0f;
				if (ray_intersects_sphere(ray, sphere, t1, t2)) {
					float closest = 1e5f;
					if ((viewport_distance_from_camera <= t1 && t1 <= max_view_distance) && t1 < closest)
						closest = t1;
					if ((viewport_distance_from_camera <= t2 && t2 <= max_view_distance) && t2 < closest)
						closest = t2;
					if (viewport_distance_from_camera <= closest && closest <= max_view_distance) {
						Point surface_position = ray.origin + closest * ray.direction;
						//surface_position = inverse4(transform) * surface_position;
						Vector surface_normal = normalized(surface_position - sphere.center);
						//surface_normal = normalized(transpose4(inverse4(transform)) * surface_normal);
						Vector eye = -ray.direction;

						const Material &material = materials[i % MATERIALS_COUNT];

						Color ambient = color(0.0f, 0.0f, 0.0f);
						Color diffuse = color(0.0f, 0.0f, 0.0f);
						Color specular = color(0.0f, 0.0f, 0.0f);

						for (int j = 0; j < LIGHTS_COUNT; j++) {
							const PointLight &light = lights[j];

							Vector light_direction = normalized(light.position - surface_position);

							Color effective_color = material.color * light.intensity;

							ambient += effective_color * material.ambient;

							float light_dot_normal = dot(light_direction, surface_normal);
							if (light_dot_normal >= 1e-5f) {
								diffuse += effective_color * material.diffuse * light_dot_normal;

								Vector reflection = -light_direction - 2.0f * surface_normal * dot(-light_direction, surface_normal);
								float reflection_dot_eye = dot(reflection, eye);
								if (reflection_dot_eye > 1e-5f) {
									float factor = powf(reflection_dot_eye, material.shininess);
									specular += light.intensity * material.specular * factor;
								}
							}
						}

						writepix(canvas, x, y, ambient + diffuse + specular);
					}
				}
			}
		}
	}

	toppm(canvas, stdout);
}
